# -*- coding: utf-8 -*-
"""
The module contains classes and functions to evaluate distribution
on causal directed acyclic graphs and their inflations.

@author: Davide Poderini
"""

import numpy as np
from sympy import lambdify
from itertools import combinations, chain
from operator import mul
from .utils import is_number, sort_symbols
from .inflation import *
from .scalar_extension import *

class Distribution:
    """Class for evaluating expected values and probabilities from a given distribution.
    
    Parameters
    ----------
    distr_array: numpy.array
        Distribution expressed as multidimensional array, where
        each axis correspond to a variable and the shape to the
        cardinality of the associated random variable.
    variables: list of sympy.Symbol
        List of Sympy symbols associated to each random variable to be evaluated.
    
    Optional
    --------
    values: list, tuple or array-like (default None)
        List of values associated with each random variables.
        If None is given it will default to {+1, -1} if the dimension
        of `distr_array` is 2 for each axis, or to {0,..,d-1} if the
        dimension is d.
    
    Notes
    -----
    The symbols for probabilities are generated automatically from the ones specified 
    in the variables list, by appending a superscript, e.g. the proability of the k-th
    outcome of variable "X" is represented by "X^k". 
    You can always use custom symbols using the `variables` argument of the eval method.
    """
    
    def __init__(self, distr_array, variables, values=None):
        dshape = distr_array.shape
        if len(dshape) != len(variables):
            raise ValueError("Number of variables do not match the distribution shape.")
        s = distr_array.sum()
        if s != 1 : 
            self.a = distr_array/s
        else:
            self.a = distr_array
        
        if values is None:
            if all(s == 2 for s in dshape):
                self.values = [np.array([1,-1])]*len(dshape)
            else:
                self.values = [np.arange(k) for k in dshape]
        else:
            if len(values) != len(dshape):
                raise ValueError("Distribution shape and values list dimensions do not match.")
            for val, d in zip(values, dshape):
                if len(val) != d:
                    raise ValueError("Distribution shape and values list dimensions do not match.")
            self.values = [np.array(val) for val in values]
        
        
        # Generate mesh and probability symbols
        self._generate_var_mesh(variables)
        self._generate_prob_mesh()
    
    def _generate_var_mesh(self, variables):
        # Generate the meshgrid for evaluation
        valmesh = np.meshgrid(*self.values, copy=False, indexing="ij")
        # Generate variable index
        self._var_mesh = {}
        for v, m in zip(variables, valmesh):
            self._var_mesh[v] = m
    
    def _generate_prob_mesh(self):
        dshape = self.a.shape
        self._prob_vec = {}
        self._prob_var = {}
        for i, v in enumerate(self.variables):
            for j in range(dshape[i]):
                p = Distribution._probability_sym_generator(v, j)
                self._prob_var[p] = v
                pvec = np.zeros(tuple(dshape[i] if k==i else 1 for k in range(len(dshape))))
                pvec[tuple(j if k==i else 0 for k in range(len(dshape)))] = 1
                self._prob_vec[p] = pvec
    @property
    def shape(self):
        return self.a.shape
    
    @property
    def variables(self):
        return [v for v in self._var_mesh.keys()]
                    
    @property
    def probabilities(self):
        return [v for v in self._prob_vec.keys()]
    
    def prob_variable(self, p):
        if not self.is_probability(p):
            raise ValueError(f"{p} is not a probability symbol.")
        return self._prob_var[p]
    
    def var_probabilities(self, v):
        if not self.is_variable(v):
            raise ValueError(f"{v} is not a variable symbol.")
        probs = []
        for p, var in self._prob_var.items():
            if var == v:
                probs.append(p)
        return probs
    
    def get_values(self, v):
        v_id = self.variables.index(v)
        return self.values[v_id]
    
    def is_variable(self, s):
        return s in self._var_mesh
    
    def is_probability(self, s):
        return s in self._prob_vec
    
    @staticmethod      
    def _probability_sym_generator(s, val):
        if isinstance(s, Symbol):
            new_name = f"{s.name}^{val}"
            new_s = Symbol(new_name, **s.assumptions0)
            
        elif isinstance(s, HermitianOperator):
            new_symbol = Distribution._probability_sym_generator(s.args[0], val)
            new_s = HermitianOperator(new_symbol)
       
        elif isinstance(s, Operator):
            new_symbol = Distribution._probability_sym_generator(s.args[0], val)
            new_s = Operator(new_symbol)
        return new_s
                
    def prob_mesh(self, s):
        return np.broadcast_to(self._prob_vec[s], self.shape)
    
    def var_mesh(self, s):
        return self._var_mesh[s]    
    
    @staticmethod         
    def _eval_nc(a, expr, eval_vars, margs):
        # Substitute every non evaluated variable by noncommutative dummy symbols.
        # This is just a trick to keep the variable order in the expression and 
        # avoid a lambdify bug when using superscripts.
        not_evaluated = {}
        i = 0
        for s in expr.free_symbols:
            if not s in eval_vars:
                not_evaluated[s] = sp.Symbol(f"_d_{i}", commutative=False)
                i += 1
        expr = expr.subs(not_evaluated)

        fexpr = sp.lambdify(eval_vars, expr, dummify=True)
        result = (a*fexpr(*margs)).sum()
    
        if not_evaluated:
            # After the evaluation, we need to convert back the 
            # non-evaluated variables to whatever they where before.
            result = result.subs({v:k for k,v in not_evaluated.items()})
        return result
    
    def eval(self, expr, variables=None):
        """Evaluates the expectation value of an expression.
        
        Parameters
        ----------
        expr: sympy.Expr
            The expression to be evaluated.
        
        Optional
        --------
        variables: List or dictionary of sympy.Symbol (default: None)
            The subset of variables in the expression to be evaluated.
            If is a dictionary, performs the substitutions defined by it before
            the evaluation.
        """
        if is_number(expr):
            return expr

        eval_vars = []
        margs = []
        for a in expr.free_symbols:
            if not variables is None:
                if a in variables:
                    m = variables[a] if isinstance(variables, dict) else a
                else:
                    continue
            else:
                m = a
            if self.is_variable(m):
                margs.append(self.var_mesh(m))
                eval_vars.append(a)
            elif self.is_probability(m):
                margs.append(self.prob_mesh(m))
                eval_vars.append(a)
        
        return self._eval_nc(self.a, expr, eval_vars, margs)

class DAGDistribution(Distribution):
    def __init__(self, distr_array, variables, dag, values=None):
        
        # Check if the variables given match the ones in the DAG
        for v in variables:
            if not dag.is_observable(v):
                raise ValueError(f"Variable {v} is not in the DAG (or is not observable).")
        self.dag = dag
        super().__init__(distr_array, variables, values=None)

    def _dag_split_symbols(self, symbols):
        dag_symbols = set()
        for s in symbols:
            if self.is_variable(s):
                dag_symbols.add(s)
            elif self.is_probability(s):
                dag_symbols.add(self.prob_variable(s))

        symbols_comp, n_comp = self.dag.d_split(dag_symbols, return_components=True)
    
        symbols_list = [[] for _ in range(n_comp)]
        for s in symbols:
            if self.is_variable(s):
                symbols_list[symbols_comp[s]].append(s)
            elif self.is_probability(s):
                symbols_list[symbols_comp[self.prob_variable(s)]].append(s)
        return symbols_list

    def eval(self, expr, variables=None):
        """Evaluates the expectation value of an expression given a distribution 
        for the variables in it and a causal DAG, using d-separation when possible.
    
        Parameters
        ----------
        expr: sympy.Expr
            The expression to be evaluated.
        dist: Distribution
            Distribution for the random variables involved. Could be an object
            of class distribution, Probability or their contextual versions.
        dag: CausalDAG
            The DAG used simplify the evaluation using d-separation, if possible.        
        """
        if not variables is None:
            eval_symbols = {} if isinstance(variables, dict) else []
            for a in expr.free_symbols:
                if a in variables:
                    if isinstance(variables, dict):
                        m = variables[a]
                        eval_symbols[m] = a
                    else:
                        m = a
                        eval_symbols.append(m)
                    
        else:
            eval_symbols = expr.free_symbols
        
        symbols_list = self._dag_split_symbols(eval_symbols)

        # Evaluate them one after the other if possible.
        for symbols in symbols_list:
            if isinstance(variables, dict):
                ev_vars = {eval_symbols[s]: s for s in symbols}
            else:
                ev_vars = symbols
            expr = super().eval(expr, variables=ev_vars)
        
        return expr

class ContextualDistribution(Distribution):
    """Class for evaluating expectation values given a set of distributions
    for different contexts (usually representing marginals).
    
    Parameters
    ----------
    distributions: Distribution
        Correlations objects for each context.
    
    Optional
    --------
    no_distrubance: Bool (default True)
        Whether to check for no-disturbance between different contexts, i.e.
        if True, it will check that the marginals of the variables represented 
        by the same Symbol in the different contexts have the same expectation values. 
    """

    def __init__(self, *distributions, no_disturbance=True):
        # Sanity checks on the distributions
        for dist in distributions:
            if not isinstance(dist, Distribution):
                raise ValueError("Arguments must be of type Distribution.")
        
        for i, d1 in enumerate(distributions):
            for d2 in distributions[i+1:]:
                if all(m in d1.variables for m in d2.variables):
                    raise RuntimeError(f"{d1} and {d2} have the same set of variables.")
        
        self.distributions = distributions
        # Build context index
        self.var_context = {}
        self.prob_context = {}
        variables = set()
        probabilities = set()
        for i, d in enumerate(self.distributions):
            for v in d.variables:
                if v in self.var_context:
                    self.var_context[v].append(i)
                else:
                    self.var_context[v] = [i]
            for p in d.probabilities:
                if p in self.prob_context:
                    self.prob_context[p].append(i)
                else:
                    self.prob_context[p] = [i]

            variables.update(d.variables)
            probabilities.update(d.probabilities)
        self._variables = sort_symbols(variables)
        self._probabilities = sort_symbols(probabilities)
        self._generate_var_mesh()
        self._generate_prob_mesh()
        
        # Check if no-disturbance is satisfied, if required
        if no_disturbance:
            if not self.check_all_no_disturbance():
                raise ValueError("The correlations do not satisfy no-disturbance, while no_disturbance is True.")
            else:
                self.no_disturbance = True
        else:
            self.no_disturbance = False
    
    def _generate_var_mesh(self):
        values = [self.get_values(v) for v in self.variables]
        valmesh = np.meshgrid(*values, copy=False, indexing="ij")
        # Generate variable index
        self._var_mesh = {}
        for v, m in zip(self.variables, valmesh):
            self._var_mesh[v] = m
    
    def _generate_prob_mesh(self):
        self._prob_vec = {}
        self._shape = []
        for i, v in enumerate(self.variables):
            probs = list(self.var_probabilities(v))
            self._shape.append(len(probs))
            for j, p in enumerate(probs):
                pvec = np.zeros(tuple(len(probs) if k==i else 1 for k in range(len(self.variables))), dtype='bool')
                pvec[tuple(j if k==i else 0 for k in range(len(self.variables)))] = 1
                self._prob_vec[p] = pvec 
    
    @property
    def shape(self):
        return self._shape
    
    @property
    def variables(self):
        return self._variables
    
    @property
    def probabilities(self):
        return self._probabilities
    
    def prob_variable(self, p):
        for d in self.distributions:
            try:
                return d.prob_variable(p)
            except ValueError:
                pass
        raise ValueError(f"{p} is not a probability symbol.")
    
    def var_probabilities(self, v):
        for d in self.distributions:
            try:
                probs = d.var_probabilities(v)
            except ValueError:
                pass
            else:
                return probs
            
        raise ValueError(f"{v} is not a variable symbol.")
    
    def get_values(self, v):
        for d in self.distributions:
            if d.is_variable(v):
                return d.get_values(v)
    
    def is_variable(self, s):
        return s in self.variables
    
    def is_probability(self, s):
        return s in self.probabilities
    
    def check_no_disturbance(self, expr, a, b, tol=1e-11):
        return self.eval(expr, context=a) - self.eval(expr, context=b) <= tol
    
    def check_all_no_disturbance(self, tol=1e-11):
        # Build the dictionary with all the expressions to check
        # and the corresponding contexts where they can be found
        contexts = [set() for _ in range(len(self.distributions))]
        for v, c_ids in self.var_context.items():
            for i in c_ids:
                contexts[i].add(v)
        
        exprs = {}
        for i, c in enumerate(contexts):
            for n in range(1, len(c)):
                for comb in combinations(c, n):
                    e = reduce(mul, comb, 1)
                    if e in exprs:
                        exprs[e].append(i)
                    else:
                        exprs[e] = [i]
        
        # Check for disturbance
        for e, contexts in exprs.items():
            for a, b in combinations(contexts, 2):
                if not self.check_no_disturbance(e, a, b, tol=tol):
                    return False
        return True            
    
    def _context_from_vars(self, vars):
        if vars[0] in self.var_context:
            possible_contexts = self.var_context[vars[0]]
        elif vars[0] in self.prob_context:
            possible_contexts = self.prob_context[vars[0]]
        else:
            raise ValueError(f"No contexts for {vars[0]}")
        
        if len(vars) == 1:
            return possible_contexts        
        
        possible_contexts = set(possible_contexts)
        for v in vars[1:]:
            if v in self.var_context:
                possible_contexts = possible_contexts.intersection(self.var_context[v])
            elif v in self.prob_context:
                possible_contexts = possible_contexts.intersection(self.prob_context[v])
            else:
                raise ValueError(f"No contexts for {vars[0]}")
        
        return list(possible_contexts)
    
    def is_evaluable(self, varset):
        return len(self._context_from_vars(varset)) > 0
     
    def eval(self, expr, variables=None, context=None):
        """Evaluates the expectation value of an expression.
        
        Parameters
        ----------
        expr: sympy.Expr
            The expression to be evaluated.
        
        Optional
        --------
        variables: List or dictionary of sympy.Symbol (default: None)
            The subset of variables in the expression to be evaluated.
            If is a dictionary, performs the substitutions defined by it before
            the evaluation.
        context: List of sympy.Symbol (default None)
        """
        if is_number(expr):
            return expr
        # Collect free variables in expr,
        # filter and substitute if necessary
        if variables:
            varset = set()
            for s in expr.free_symbols:
                if s in variables:
                    var = variables[s] if isinstance(variables, dict) else s
                    varset.add(var)
        else:
            varset = expr.free_symbols
        filtered_varset = varset.intersection(self.variables + self.probabilities)
        
        # Look for the right context
        # if given in terms of symbols, or not given at all, try to guess the right one
        if not isinstance(context, int):
            if context:
                possible_contexts = self._context_from_vars(list(filtered_varset.union(context)))
            else:
                possible_contexts = self._context_from_vars(list(filtered_varset))
            
            if len(possible_contexts) == 0 or \
                    not self.no_disturbance and len(possible_contexts) > 1:
                # if no-disturbance is false, we cannot tolerate ambiguities
                warn("No context found for the specified variables.")
                return expr
            
            context = possible_contexts[0]

        distr = self.distributions[context]
        return distr.eval(expr, variables=variables)

class InflatedDistribution(Distribution):
    def __init__(self, base_distribution, inflation):
        # Check if the variables given match the ones in the DAG
        for v in base_distribution.variables:
            if not inflation.base_dag.is_observable(v):
                raise ValueError(f"Could not find observable node {v} in the DAG.")
        
        self.base_distribution = base_distribution
        self.inflation = inflation
        self.inflated_variables = []
        self.inflated_prob_base = {}
        self.inflated_prob_var = {}
        for bn in base_distribution.variables:
            dim = len(self.base_distribution.get_values(bn))
            for cn in inflation.get_copies(bn):
                self.inflated_variables.append(cn)
                for i in range(dim):
                    p = Distribution._probability_sym_generator(cn, i)
                    bp = Distribution._probability_sym_generator(bn, i)
                    self.inflated_prob_var[p] = cn
                    self.inflated_prob_base[p] = bp             
        self._generate_var_mesh()
        self._generate_prob_mesh()
    
    def _generate_var_mesh(self):
        values = [self.get_values(v) for v in self.variables]
        valmesh = np.meshgrid(*values, copy=False, indexing="ij")
        # Generate variable index
        self._var_mesh = {}
        for v, m in zip(self.variables, valmesh):
            self._var_mesh[v] = m
    
    def _generate_prob_mesh(self):
        self._prob_vec = {}
        self._shape = []
        for i, v in enumerate(self.variables):
            probs = list(self.var_probabilities(v))
            self._shape.append(len(probs))
            for j, p in enumerate(probs):
                pvec = np.zeros(tuple(len(probs) if k==i else 1 for k in range(len(self.variables))), dtype='bool')
                pvec[tuple(j if k==i else 0 for k in range(len(self.variables)))] = 1
                self._prob_vec[p] = pvec 
    @property
    def dag(self):
        return self.inflation.dag
    
    @property
    def shape(self):
        return self._shape

    @property
    def variables(self):
        return self.inflated_variables
    
    @property
    def probabilities(self):
        return [v for v in self.inflated_prob_var.keys()]

    def prob_variable(self, p):
        if not self.is_probability(p):
            raise ValueError(f"{p} is not a probability symbol.")
        return self.inflated_prob_var[p]
    
    def var_probabilities(self, v):
        if not self.is_variable(v):
            raise ValueError(f"{v} is not a variable symbol.")
        for p, var in self.inflated_prob_var.items():
            if var == v:
                yield p
    
    def get_values(self, v):
        bv = self.inflation.base_variable(v)
        return self.base_distribution.get_values(bv)
                   
    def is_variable(self, v):
        return v in self.inflated_variables

    def is_probability(self, v):
        return v in self.inflated_prob_var
    
    def get_copies(self, s):
        if s in self.base_distribution.variables:
            return self.inflation.get_copies(s)
        elif s in self.base_disribution.probabilities:
            copies = []
            for ip, bp in self._inflated_prob_base.items():
                if s == bp:
                    copies.add(ip)
            return copies
        else:
            raise ValueError(f"InvalidSymbol {s}")

    def _d_split_symbols(self, symbols):
        dag_symbols = set()
        for s in symbols:
            if self.is_variable(s):
                dag_symbols.add(s)
            elif self.is_probability(s):
                dag_symbols.add(self.prob_variable(s))

        symbols_comp, n_comp = self.inflation.dag.d_split(dag_symbols, return_components=True)
    
        symbols_list = [[] for _ in range(n_comp)]
        for s in symbols:
            if self.is_variable(s):
                symbols_list[symbols_comp[s]].append(s)
            elif self.is_probability(s):
                symbols_list[symbols_comp[self.prob_variable(s)]].append(s)
        return symbols_list

    def is_injectable(self, symbols):
        varset = set()
        for s in symbols:
            if self.is_probability(s):
                varset.add(self.prob_variable(s))
            else:
                varset.add(s)
        return self.inflation.is_injectable(varset)
        
    def inject(self, symbols):
        inj_symbols = []
        if self.is_injectable(symbols):
            for s in symbols:
                if self.is_probability(s):
                    inj_symbols.append(self.inflated_prob_base[s])
                elif self.is_variable(s):
                    inj_symbols.append(self.inflation.base_variable(s))
            return inj_symbols
        else:
            raise NotInjectable(symbols)
    
    def is_evaluable(self, varset):
        symbols_list = self._d_split_symbols(varset)
        evaluable = True
        for symbols in symbols_list:
            evaluable = evaluable and self.is_injectable(symbols)
            if is_contextual(self.base_distribution):
                    inj_symbols = self.inject(symbols)
                    evaluable = evaluable and self.base_distribution.is_evaluable(inj_symbols)
            if not evaluable:
                return False
        return evaluable

    def eval(self, expr, variables=None, break_separable=True):
        """Evaluates the expectation value of an expression given a distribution 
        for the variables in it and an inflation of a causal DAG, checking for injectability
        and using d-separation if break_separable is True.
    
        Parameters
        ----------
        expr: sympy.Expr
            The expression to be evaluated. The symbols representing the variables
        dist: Distribution
            Distribution for the random variables involved. Could be an object
            of class distributions, Probability or their contextual versions.
        inflation: Inflation
            The Inflated DAG that will be used to check for injectability and simplify 
            the evaluation using d-separation, if possible.
    
        Optional
        --------
        break_separable: Bool (default True)
            Whether to check for d-separation in the Inflated DAG before checking for injectability.
        """
        if not variables is None:
            eval_symbols = []
            for a in expr.free_symbols:
                if a in variables:
                    m = variables[a] if isinstance(variables, dict) else a
                    eval_symbols.append(m)
        else:
            eval_symbols = expr.free_symbols
        
        if break_separable:
            symbols_list = self._d_split_symbols(eval_symbols)
        else:
            symbols_list = list(filter(lambda s: self.is_variable(s) or self.is_probability(s), eval_symbols))
        # Evaluate them one after the other if possible.
        for symbols in symbols_list:
            try:
                bnodes = self.inject(symbols)
                expr = self.base_distribution.eval(expr, variables={a:b for a,b in zip(symbols, bnodes)})
            except NotInjectable:
                pass
    
        return expr
    
def is_dag_constrained(distribution):
    return isinstance(distribution, DAGDistribution) or\
            isinstance(distribution, ContextualDistribution) and\
             isinstance(distribution.distributions[0], DAGDistribution)

def is_contextual(distribution):
    return isinstance(distribution, ContextualDistribution) or\
            isinstance(distribution, InflatedDistribution) and\
             isinstance(distribution.base_distribution, ContextualDistribution)

def is_inflation_constrained(distribution):
    return isinstance(distribution, InflatedDistribution)