from ncpol2sdpa.nc_utils import get_all_monomials
from ncpol2sdpa import SdpRelaxation
from .scalar_extension import *
from .inflation import CausalDAG, Inflation
from .distributions import *
from .utils import sort_symbols, convert_subs_to_copy_index, copy_index_var_commutations, is_number
from picos.modeling import SolutionFailure
from functools import reduce
from operator import mul

from cvxopt import matrix, spmatrix, solvers
from mosek import iparam

class OptimizationProblem:
    pass

class FeasibilityProblem:
    pass

class LPOptimizationProblem:
    pass

class InflationFeasibilityProblem:
    def __init__(self, distribution, 
                 solver='mosek', verbose=0, remove_degeneracy=True, exclude_probs=None):
        if not solver in ['cvxopt', 'mosek']:
            raise ValueError("Sorry, only cvxopt and mosek are supported.")
        self._distribution = None
        self.distribution = distribution
        self.exclude_probs = exclude_probs
        self.remove_degeneracy = remove_degeneracy
        
        self.verbose = verbose
        self.solver = solver
    
    def _filter_probs(self, eset, filter_esets):
        filter_out_probs = set()
        if not filter_esets:
            # Just get out the first element to account for normalization
            fps = tuple(next(self.distribution.var_probabilities(v)) for v in sort_symbols(eset))
            filter_out_probs.add(fps)
        else:
            for feset in filter_esets:
                common_vars = eset.intersection(feset)
                diff_vars = eset.difference(feset)
                fixed_probs = []
                fps = []
                for v in sort_symbols(eset):
                    fixed_probs.append(next(self.distribution.var_probabilities(v)))
                    if v in common_vars:
                        fps.append(list(self.distribution.var_probabilities(v)))
                for fprobs in product(*fps):
                    already_filtered = False
                    for p in self._explicitly_discarded:
                        if set(fprobs).issubset(p) and set(fixed_probs).issubset(p):
                            already_filtered = True
                            break
                    if not already_filtered:
                        filter_out_probs.add(tuple(fixed_probs + list(fprobs)))
        return filter_out_probs
    
    def sym_joint_prob(self):
    
        inf_vars = self.distribution.variables
        inf_probs = []
        for k, v in enumerate(inf_vars):
            inf_probs.append(list(self.distribution.var_probabilities(v)))
        jprobs = []
        for probs in product(*inf_probs):
            p = reduce(mul, probs)
            jprobs.append(p)
        return jprobs
  
    def _build_M(self):
    
        if self.verbose: print(f"Generating M ", end="...")
        shape = self.distribution.shape
        m = reduce(mul, shape)
        # Create function to explicitly exclude probabilities
        if self.exclude_probs:
            if callable(self.exclude_probs):
                f_exclude = self.exclude_probs
            else:
                f_exclude = lambda x: x in self.exclude_probs
        else:
            f_exclude = lambda x: False
    
        n = 1
        J = np.arange(m)
        I = np.full(m, 0)
        self.sym_b = [1,]
        
        for i, es in enumerate(self.distribution.inflation.expressible_sets):
            
            # Variable set filtering
            if is_contextual(self.distribution):
                if not self.distribution.is_evaluable(es):
                    print(es, "was excluded for context")
                    continue
            
            if self.remove_degeneracy:
                fprobs = self._filter_probs(es, self.distribution.inflation.expressible_sets[:i])
            
            for probs in product(*[self.distribution.var_probabilities(evar) for evar in sort_symbols(es)]):
                
                # Probability filtering
                if (self.remove_degeneracy and tuple(probs) in fprobs) or\
                         f_exclude(probs):
                    continue
                
                # Build row
                expr = reduce(mul, probs)
                self.sym_b.append(expr)
                row = True
                for p in probs:
                    row *= self.distribution.prob_mesh(p)
                arg_js = np.argwhere(row.ravel() == True).ravel()
    
                J = np.hstack((J, arg_js))
                I = np.hstack((I, np.full(arg_js.size, n)))
                n += 1
                if self.verbose: print(f"\rGenerating M, {n} rows generated ", end="...")
        
        if self.verbose: print(" Done")
        self._I = I
        self._J = J
        self._shape = (n,m)
    
    def _clear_M(self):
        self._I = None
        self._J = None
        self._shape = None
    
    @property
    def M(self):
        """The sparse description of the marginal problem matrix M,
        as array of row and column indices, I and J.
        """
        if self._I is None or self._J is None or self._shape is None:
            self._build_M()
        return self._I, self._J
    
    @property
    def shape(self):
        """The shape of the marginal problem matrix M,
        as a tuple containing row and column number respectively.
        """
        if self._I is None or self._J is None or self._shape is None:
            self._build_M()
        return self._shape

    def _build_b(self):
        if self.verbose: print(f"Calculating b ", end="...")
        b = np.empty((self.shape[0], ))
        b[0] = 1
        k = 1
        l = 1
        for i, p_expr in enumerate(self.sym_b[1:]):
            marg = self.distribution.eval(p_expr)
            if not is_number(marg):
                raise RuntimeError(f"Expression {expr} does not evaluate to a number (evaluated expression: {marg})")
            b[i+1] = marg
            if self.verbose: print(f"\rCalculating b: {i+2}/{self.shape[0]} rows evaluated", end="...")
        if self.verbose: print(" Done")
        self._b = b
    
    def _clear_b(self):
        self._b = None
        self._sym_b = None
    
    @property
    def b(self):
        """The evaluated marginal vector of the problem.
        """
        if self._b is None:
            self._build_b()
        return self._b
    
    @property
    def distribution(self):
        """The distribution to test for feasibility.
        Must be of type InflatedDistribution.
        """
        return self._distribution
    
    @distribution.setter
    def distribution(self, distribution):
        if not is_inflation_constrained(distribution):
            raise ValueError(f"Only causal constraint of type Inflation are allowed.")
        if self._distribution:
            if self._distribution != distribution:
                if self._distribution.inflation != distribution.inflation:
                    self._clear_M()
                self._clear_b()
        else:
            self._clear_M()
            self._clear_b()
            
        self._distribution = distribution
        self._solved = False
        self._feasible = None
        self.problem = None
    
    # Read-only propertiesfeasibility
    @property
    def solved(self):
        return self._solved
    @property
    def feasible(self):
        return self._feasible
    @property
    def status(self):
        if self.solved:
            return self.problem['status']
        else:
            return None
    @property
    def primal_solution(self):
        if self.solved:
            return self.problem['x']
        else:
            return None
    @property
    def dual_solution(self):
        if self.solved:
            return self.problem['y']
        else:
            return None
    @property
    def solution(self):
        if self.solved:
            return self.primal_solution
        else:
            return None
    
    @property
    def certificate_array(self):
        if self.solved and not self.feasible:
            return np.array(self.dual_solution)
        else:
            return None
    
    def get_symbolic_certificate(self, round=None):
        y = self.certificate_array 
        if not y is None:
            if round:
                y = y.round(round)
            return (self._sym_b @ y).sum()
        else:
            return None
    
    def solve(self):
        """Solve the feasibility problem.
        """
        if self.verbose > 1:
            solvers.options['show_progress'] = True
        else:
            solvers.options['show_progress'] = False
            solvers.options['mosek'] = {iparam.log: 0}
        
        I, J = self.M
        M = spmatrix(1, I, J, self.shape)
        # Positivity
        G = -spmatrix(1, np.arange(self.shape[1]), np.arange(self.shape[1]))
        z = matrix(0.0, (self.shape[1], 1))
        b = matrix(self.b)
        
        self.problem = solvers.lp(z, G, z, M, b, solver=self.solver)
        if self.problem['status'] == 'optimal':
            self._solved = True
            self._feasible = True
        elif self.problem['status'] == 'primal infeasible':
            self._solved = True
            self._feasible = False
        else:
            self._solved = False
            self._feasible = None
        
class NPAOptimizationProblem:
    pass

class NPAFeasibilityProblem:
   
    def __init__(self, distribution, 
                level=1, scalar_extension=None, substitutions=None, solver='cvxopt', verbose=False):
        
        self._base_substitutions = substitutions
        self.distribution = distribution
               
        if isinstance(scalar_extension, list):
            αs = generate_variables("α", n_vars=len(scalar_extension))
            self._scalar_extension = {m:α for m, α in zip(scalar_extension, αs)}
        elif isinstance(scalar_extension, int) and scalar_extension > 0:
            if scalar_extension > level: 
                scalar_extension = level
            self._scalar_extension = scalar_extension
        elif scalar_extension == "all":
            self._scalar_extension = level
        else:
            self._scalar_extension = None
        
        self.level = level
        
        if not solver in ["cvxopt", "mosek"]:
            raise ValueError("Unsupported solver: {solver}.")
        self.solver = solver
        self.verbose = verbose
        
        self._se_subsfun = None
        self._se_vars = None
    
    # Read-only properties
    @property
    def solved(self):
        return self._solved
    @property
    def feasible(self):
        return self._feasible
    @property
    def status(self):
        return self.sdp.status
    @property
    def primal(self):
        return self.sdp.primal
    @property
    def dual(self):
        return self.sdp.dual
    @property
    def primal_solution(self):
        return self.sdp.x_mat
    @property
    def dual_solution(self):
        return self.sdp.y_mat
    @property
    def solution(self):
        if self.solved:
            return self.sdp.x_mat
        else:
            return None
    @property
    def distribution(self):
        return self._distribution
    
    @distribution.setter
    def distribution(self, distribution):
        if is_inflation_constrained(distribution):
            self.substitutions = convert_subs_to_copy_index(self._base_substitutions, distribution.inflation)
            self.substitutions.update(copy_index_var_commutations(distribution.inflation))
        elif is_dag_constrained(distribution):
            self.substitutions = self._base_substitutions
        else:
            raise ValueError(f"Only causally constrained Distributions of type DAGDistribution or InflatedDistribution are allowed.")
        
        self._distribution = distribution
        self.sdp = None
        self._solved = False
        self._feasible = None
    
    def using_scalar_extension(self):
        return isinstance(self._scalar_extension, int) or\
                 isinstance(self._scalar_extension, dict)
    
    def _generate_eval_function(self):
        efun = self.distribution.eval
        return efun
    
    def _generate_se_substitution_function(self):
        if not self.using_scalar_extension():
            self._se_subsfun = None
            self._se_vars = None
        else:
            dep_graph = self.distribution.dag.dependency_graph
            sep_graph = complement_graph(dep_graph)
            
            # Generate or collect the extension variables
            if isinstance(self._scalar_extension, int):
                g = build_separability_graph(sep_graph, self.substitutions, self._scalar_extension, verbose=self.verbose)
                extension_variables = get_scalar_extension_variables(g)
                #monomials = [g.vp.var[v] for v in g.vertices()]
            elif isinstance(self._scalar_extension, dict):
                extension_variables = self._scalar_extension
                variables = list(self._scalar_extension.keys())
                #monomials = get_all_monomials(variables, None, self.substitutions, self.level)
            else:
                raise ValueError("Invalid extension variables specification.")
        
            subfunc = partial(scalar_extension_substitute, 
                dependency_graph=dep_graph, 
                extension_variables=extension_variables,
                substitutions=self.substitutions)
            self._se_subsfun = subfunc
            self._se_vars = extension_variables
    
    def _generate_substitution_function(self):
        self.applied_subs = {}
        eval_fun = self._generate_eval_function()
        if self.using_scalar_extension():
            if self._se_subsfun is None or self._se_vars is None:
                self._generate_se_substitution_function()
            
            exvars_eval = {} 
            for m, α in self._se_vars.items():
                s = eval_fun(m)
                if s != m:
                    exvars_eval[α] = s 
            def subsfun(m):
                oldm = m
                # Apply extension variables substitutions
                m = self._se_subsfun(m)
                # Evaluate extension variables
                m = m.subs(exvars_eval)
                # Compute probability if possible
                s = eval_fun(m)
                self.applied_subs[oldm] = s
                return s
        else:
            subsfun = eval_fun
        return subsfun
    
    def get_all_variables(self):
        return [m for n in self.distribution.dag.get_nodes(return_all=True) for m in n]
    
    def generate_relaxation(self):
        if self.verbose: print("Computing substitution function...")
        subfunc = self._generate_substitution_function()
        if self._se_vars:
            extravars = [m for m in self._se_vars.values()]
        else:
            extravars = None
        if self.verbose: print("Generating SDP relaxation...")
        self.sdp = SdpRelaxation(self.get_all_variables(), verbose=self.verbose)
        self.sdp.get_relaxation(self.level, objective=0,
                                 substitutions=self.substitutions,
                                 momentsubstitutions=subfunc,
                                 extramonomials=extravars,
                                )
    # TODO: cleaner adaptation to solvers
    def solve(self):
        """Check the feasibility of a distribution for a given
        DAG or inflation at a specific NPA level.
        
        Return
        ------
        True if is feasible, False otherwise. 
        """
        if self.sdp is None:
            self.generate_relaxation()        
        if self.verbose: print("Solving...")
        try:
            self.sdp.solve(solver=self.solver)
        except SolutionFailure as e:
            if e.code == 3:
                self._solved = True
                self._feasible = False
                if self.verbose: print(f"Unfeasible")
            elif self.verbose:
                print(e)
        except RuntimeError as e:
            if e.args[0] == 'Problem variable dimension is zero' and self.sdp.n_vars == 0:
                if self.sdp.get_symbolic_matrix().is_positive_semidefinite:
                    self._solved = True
                    self._feasible = True
                    if self.verbose: print(f"Feasible")
                else:
                    self._solved = True
                    self._feasible = False
                    if self.verbose: print(f"Unfeasible")
        else:
            if self.sdp.status == 'optimal':
                self._solved = True
                self._feasible = True
                if self.verbose: print(f"Feasible")
            else:
                if self.solver == 'mosek' and (self.sdp.status == 'dual_infeas_cer' or self.sdp.status == 'prim_infeas_cer'):
                    self._solved = True
                    self._feasible = False
                    if self.verbose: print(f"Status: {self.sdp.status}")    
                else:
                    if self.verbose: print(f"Unknown, status: {self.sdp.status}")    
        return self.feasible
