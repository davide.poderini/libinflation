# -*- coding: utf-8 -*-
"""
The module contains classes to define Causal directed acyclic graphs
and their inflations.

@author: Davide Poderini
"""
from warnings import warn
from graph_tool import Graph, GraphView
from graph_tool.topology import *
from graph_tool.util import find_vertex
from graph_tool.generation import complete_graph
try:
    from graph_tool.draw import graph_draw
except ValueError:
    warn("Could not import graph drawing module.")

from sympy import Symbol, Mul, Add, Pow
from numpy import array
from sympy.physics.quantum import Operator, HermitianOperator
from itertools import combinations, product, chain

from collections.abc import Iterable
from .utils import str_index_generator, sym_index_generator, index_generator

#
## Exceptions
#

class InvalidGraph(Exception):
    def __init__(self, *args, invalid_object=None):
        super().__init__(*args)
        self.invalid_object = invalid_object

class NotDAG(InvalidGraph):
    def __init__(self, graph=None):
        super().__init__("The graph is not a DAG.", invalid_object=graph)

class InvalidInflation(InvalidGraph):
    def __init__(self, *args, invalid_object=None, invalid_operation=None):
        super().__init__(*args, invalid_object=invalid_object)
        self.invalid_operation = invalid_operation

class NotInjectable(Exception):
    def __init__(self, nodes):
        super().__init__(f"Set {nodes} is not injectable.")
        self.nodes = nodes

#
## Base classes
#

class CausalDAG(Graph):
    """Class to store a causal model represented as a Directed Acyclic Graph (DAG).
    Nodes of the graph can be indexed, retrieved and manipulated by any 
    hashable object (usually a sympy expression).
    
    Internally uses graph_tool to store the actual graph
    
    Glossary:
        "variable": Hashable index in node_index. Each of them is supposed to represent a 
                corresponding random variable. 
        "node": Nodes in the graph. They are represented by a list of variables, to allow for 
                nodes with more than one input. For convenience it can be manipulated using 
                any of its variables as handle.
        "link": Causal link between nodes. Stored as a directed edge in the graph.
        "vertex": Internal graph_tool vertex object. Is not supposed to be manipulated directly.
        "edge": Internal graph_tool edge object. Is not supposed to be manipulated directly.
    """
    
    def __init__(self, dag=None, node_index=None):
        """Create a new causal DAG.
        
        If dag and node_index are given it creates a graph based on that structures.
        """
        
        if not dag is None:
            # Basic sanity checks for the given graph
            if not is_DAG(dag):
                raise NotDAG(dag)
            if node_index is None:
                raise ValueError("External graph given but no node index specified.")
            else:
                vertices = list(dag.vertices())
                indexed_vertices = set(node_index.values())
                for v in vertices:
                    if not v in indexed_vertices:
                        raise ValueError("Vertex {v} not indexed.")
            # Create the graph and assign the index
            super().__init__(dag, directed=True)
            # We are creating both a dict and a vertex property for fast access in both directions
            self.node_index = node_index
            self.vp.node = self.new_vertex_property('object', val=None)           
            for n, v in node_index.items():
                if self.vp.node[v] is None:
                    self.vp.node[v] = [n]
                else:
                    self.vp.node[v].append(n)
            # The latent property should have been copied if there was one in the
            # given dag,if not, create it
            if not 'latent' in self.vp:
                self.vp.latent = self.new_vertex_property('bool')           
        else:
            super().__init__(directed=True)
            # We are creating both a dict and a vertex property for fast access in both directions
            self.vp.node = self.new_vertex_property('object')           
            self.node_index = {}
            self.vp.latent = self.new_vertex_property('bool')
        self._reset_dependency_graph()
    
    
    def _vertex(self, n):
        if not n in self.node_index:
            raise ValueError(f"Node {n} does not exist.")
        else:
            return self.node_index[n]
    
    def _vertex_or_add(self, n):
        if n in self.node_index:
            return self.node_index[n]
        else:
            return self.add_node(n)
            
    def _update_node_index(self):
        new_index = {}
        for v in self.vertices():
            node = self.vp.node[v]
            for n in node: 
                new_index[n] = v
        self.node_index = new_index
    
    def _checkDAG(self):
        if not is_DAG(self):
            raise NotDAG(self)
    
    def get_node_property(self, n, prop):
        vn = self._vertex(n)
        return self.vp[prop][vn]
    
    def set_node_property(self, n, prop, value):
        vn = self._vertex(n)
        self.vp[prop][vn] = value
    
    def add_node(self, node, latent=False):
        """Add node `node` to the graph, where `node`
        is an hashable object functioning as index.
        If `node` is a list map each element to a single
        vertex as different settings.
        
        Parameters
        ==========
        node: hashable type or list of hashable type
            Index of the node.
        latent: bool
            Specify if the node represents a latent variable.
        
        Returns
        =======
            v: graph_tool.vertex
                The graph_tool vertex object just added.        
        """
        if isinstance(node, Iterable):
            node = list(node)
        else:
            node = [node]
        for n in node:
            if n in self.node_index:
                raise ValueError(f"Node {n} is already present.")
        v = self.add_vertex(n=1)
        for n in node:
            self.node_index[n] = v
        self.vp.node[v] = node
        self.vp.latent[v] = latent
        self._reset_dependency_graph()
        return v
    
    def add_node_input(self, node, input):
        v = self._vertex(node)
        self.node_index[input] = v
        self.vp.node[v].append(input)
    
    def add_link(self, s, t):
        vs = self._vertex_or_add(s)
        vt = self._vertex_or_add(t)
        super().add_edge(vs, vt, add_missing=False)
        self._checkDAG()
        self._reset_dependency_graph()
    
    def add_link_list(self, link_list):
        edge_list = []
        for s, t in link_list:
            vs = self._vertex_or_add(s)
            vt = self._vertex_or_add(t)
            edge_list.append((vs, vt))
        self.add_edge_list(edge_list)
        self._checkDAG()
        self._reset_dependency_graph()
    
    def remove_node(self, node):
        if isinstance(node, Iterable):
            vertices = set()
            variables = set()
            for n in node:
                try:
                    v = self._vertex(n)
                except ValueError:
                    warn("Cannot find node {n}.")
                else:
                    vertices.add(v)
                    for idx in self.get_variables(n):
                        self.node_index.pop(idx, None)
            self.remove_vertex(list(vertices))
        else:
            v = self._vertex(node)
            for idx in self.get_variables(node):
                self.node_index.pop(idx, None)
            self.remove_vertex(v)
        # After removal graph_tool vertex descriptor may be
        # invalidated so we need to refresh the node_index
        self._update_node_index()
        self._reset_dependency_graph()
                           
    def remove_link(self, n1, n2):
        v1, v2 = tuple(self._vertex(v) for v in (n1, n2)) 
        self.remove_edge(self.edge(v1, v2))
        self._reset_dependency_graph()
    
    def parents(self, n):
        for vpar in self.get_in_neighbors(self._vertex(n)):
            yield self.vp.node[vpar][0]
    
    def children(self, n):
        for vchil in self.get_out_neighbors(self._vertex(n)):
            yield self.vp.node[vchil][0]
    
    def ancestors(self, n):
        nodes = [n]
        ancestors = set()
        while nodes:
            node = nodes.pop()
            for p in self.parents(node):
                nodes.append(p)
                ancestors.add(p)
        return ancestors
    
    def descendants(self, n):
        nodes = [n]
        descendants = set()
        while nodes:
            node = nodes.pop()
            for p in self.children(node):
                nodes.append(p)
                descendants.add(p)
        return descendants
    
    def is_latent(self, n):
        v = self._vertex(n) 
        return bool(self.vp.latent[v])
    
    def is_observable(self, n):
        return not self.is_latent(n)
    
    def set_latent(self, n):
        v = self._vertex(n)
        self.vp.latent[v] = True
        self._reset_dependency_graph()
    
    def set_observed(self, n):
        v = self._vertex(n)
        self.vp.latent[v] = False
        self._reset_dependency_graph()
    
    def get_variables(self, node):
        v = self._vertex(node)
        return self.vp.node[v]
    
    def get_nodes(self, node_type='observed', return_all=False, flatten=False):
        """ Iterate over all the nodes.
        Iterates only over observed nodes by default, the behavior
        can be changed using the keyword argument `node_type`, that
        takes "all", "observed" or "latent" as possible values.
        """
        if node_type == 'observed':
            include_node = lambda n: not self.is_latent(n)
        elif node_type == 'latent': 
            include_node = lambda n: self.is_latent(n)
        elif node_type == 'all': 
            include_node = lambda n: True
        else:
            raise ValueError("Allowed values for node_type are only 'observed', 'latent' or 'all'")
        for v in self.iter_vertices():
            node = self.vp.node[v]
            if include_node(node[0]):
                if return_all:
                    if flatten:
                        for n in node:
                            yield n
                    else:
                        yield node
                else:
                    yield node[0]
    
    def copy(self):
        new = CausalDAG(self, self.node_index)
        return new
        
    def d_separated(self, a, b, x=None):
        A = set(a) if isinstance(a, Iterable) else set({a})
        B = set(b) if isinstance(b, Iterable) else set({b})
        if x is None:
            X = set()
        else:
            X = set(x) if isinstance(x, Iterable) else set({x})
        
        ABX = A.union(B).union(X)
        ABX_vs = set()
        for n in ABX:
            ABX_vs.add(self._vertex(n))
            
        # Recursively remove leaves[item for sublist in l for item in sublist]
        fsize = self.get_vertices().size
        fg = GraphView(self, vfilt=lambda v: v.out_degree() > 0 or v in ABX_vs)       
        while fg.get_vertices().size < fsize:
            fsize = fg.get_vertices().size
            fg = GraphView(fg, vfilt=lambda v: v.out_degree() > 0 or v in ABX_vs)       
        
        # Remove out edges on X nodes
        filter_x_out = fg.new_edge_property('bool', val=True)
        for n in X:
            xv = self._vertex(n)
            for e in xv.out_edges():
                filter_x_out[e] = False
        
        # Check weakly connectect components
        ug = GraphView(fg, efilt=filter_x_out, directed=False)
        comp, _ = label_components(ug)
        comp_A = set(comp[self._vertex(n)] for n in A)
        comp_B = set(comp[self._vertex(n)] for n in B)
        
        return not comp_A.intersection(comp_B)
                
    def _build_dependency_graph(self):
        dep_graph = Graph(directed=False)
        dep_graph.vp.var = dep_graph.new_vertex_property('object')
        dg_index = {}
        # Add vertices
        for node in self.get_nodes(return_all=True):
            for n in node:
                dv = dep_graph.add_vertex()
                dep_graph.vp.var[dv] = n
                dg_index[n] = dv
            # Edges between variables representing settings
            dep_graph.add_edge_list([(dg_index[n], dg_index[m]) for n,m in combinations(node, 2)])
        
        # Add edges
        for node1, node2 in combinations(self.get_nodes(return_all=True), 2):
            if not self.d_separated(node1[0], node2[0]):
                for n1, n2 in product(node1, node2):
                    dep_graph.add_edge(dg_index[n1], dg_index[n2])
        
        self._dependency_graph = dep_graph
        self._dependency_graph_index = dg_index
    
    def _reset_dependency_graph(self):
        self._dependency_graph = None
        self._dependency_graph_index = None
    
    @property
    def dependency_graph(self):
        if self._dependency_graph is None or self._dependency_graph_index is None:
            self._build_dependency_graph()
        return self._dependency_graph 
    
    @property
    def dependency_graph_index(self):
        if self._dependency_graph is None or self._dependency_graph_index is None:
            self._build_dependency_graph()
        return self._dependency_graph_index
    
    def d_split(self, nodes, return_components=False):
        vid = [self.dependency_graph_index[n] for n in nodes]
        fdg = GraphView(self.dependency_graph, vfilt=lambda v: v in vid)
        comp, hist = label_components(fdg)
        if return_components:
            components_dict = {}
            for v in fdg.vertices():
                components_dict[fdg.vp.var[v]] = comp[v]
            return components_dict, len(hist)
        else:
            splitted = [[] for _ in range(len(hist))]
            for v in fdg.vertices():
                splitted[comp[v]].append(fdg.vp.var[v])
            return splitted
    
    observable_color = "#1982c4" 
    input_color = "#8ac926"
    latent_color = "#6a4c93"
    
    def draw(self, **kwargs):
        # Represent nodes with multiple inputs adding an 
        # appropriate input variable.
        multiple_input_nodes = []
        for v in self.iter_vertices():
            k = len(self.vp.node[v])
            if k > 1:
                multiple_input_nodes.append((v, k))
        
        if multiple_input_nodes:
            g = super().copy()
            text = g.new_vertex_property("string", val="")
            color = g.new_vertex_property('string', val=self.observable_color)
            for v, k in multiple_input_nodes:
                iv = g.add_vertex()
                g.add_edge(iv, v)
                text[iv] = str(k)
                color[iv] = self.input_color
        else:
            g = self
            text = g.new_vertex_property("string")
            color = g.new_vertex_property('string', val=self.observable_color)
        
        vshape = g.new_vertex_property('string', val="circle")
        for n in self.get_nodes(node_type='all'):
            v = self._vertex(n)
            text[v] = str(n)
            if self.vp.latent[v]:
                vshape[v] = "triangle"
                color[v] = self.latent_color
        vfontsize = g.new_vertex_property('int', val=19)
        vprops = {'text': text, 
                  'shape': vshape,
                  'fill_color': color,
                  'font_size': vfontsize}
        
        if 'vprops' in kwargs:
            kwargs['vprops'].update(vprops)
        else:
            kwargs['vprops'] = vprops
        
        return graph_draw(g, **kwargs)

class Inflation:
    
    def __init__(self, base_dag, index_generator=None):
        if not isinstance(base_dag, CausalDAG):
            raise InvalidGraph("The base DAG should be an instance of \"CausalDAG\" class")
        
        self.base_dag = base_dag
        if index_generator is None:
            self._generate_index = sym_index_generator
        else:
            try:
                test_s = list(base_dag.node_index.keys())[0]
                new_test_s = index_generator(s, 1)
            except Exception:
                raise RuntimeError(f"Invalid index generator function for index type {type(test_s)}")
            else:
                self._generate_index = index_generator
        
        self._copy_index = {n: [] for n in self.base_dag.node_index.keys()}
        self.dag = base_dag.copy()
        self.dag.vp.base_node = self.dag.new_vertex_property('object')
        new_node_index = {}
        for n in self.base_dag.get_nodes(node_type='all'):
            v = self.dag._vertex(n)
            bnode = self.base_dag.get_variables(n)
            self.dag.vp.base_node[v] = bnode
            self.dag.vp.node[v] = []
            for m in bnode:
                m0 = self._generate_index(m, 0)
                self.dag.vp.node[v].append(m0)
                self._copy_index[m].append(m0)
                new_node_index[m0] = v
        self.dag.node_index = new_node_index      
        
        self._clear_injectable_sets()
        self._clear_expressible_sets()
        
    def _generate_next_index(self, n):
        bnode = self.base_node(n)
        new_node = []
        for bn in bnode:
            new_i = len(self._copy_index[bn])
            new_node.append(self._generate_index(bn, new_i))
        return new_node    
    
    @property
    def dependency_graph(self):
        return self.dag.dependecy_graph
    
    def get_nodes(self, *args, **kwargs):
        return self.dag.get_nodes(*args, **kwargs)
        
    def base_node(self, n):
        return self.dag.get_node_property(n, 'base_node')
    
    def base_variable(self, n):
        n_id = self.dag.get_variables(n).index(n)
        return self.base_node(n)[n_id]
    
    def get_copies(self, bn):
        return self._copy_index[bn]
    
    def duplicate_node(self, n):
        new_node = self._generate_next_index(n)
        v = self.dag.add_node(new_node, latent=self.dag.is_latent(n))
        bnode = self.base_node(n)
        self.dag.vp.base_node[v] = bnode
        for bn, nn in zip(bnode, new_node):
            self._copy_index[bn].append(nn)
        
        for p in self.dag.parents(n):
            self.dag.add_link(p, new_node[0])
        
        self._clear_injectable_sets()
        self._clear_expressible_sets()
        return new_node
    
    def switch_parent(self, n, old_parent, new_parent):
        if not old_parent in list(self.dag.parents(n)):
            raise ValueError(f"{old_parent} is not a parent node of {n}.")
        self.dag.remove_link(old_parent, n)
        self.dag.add_link(new_parent, n)
        self._clear_injectable_sets()
        self._clear_expressible_sets()
    
    def merge_node(self, n1, n2, safe=True):
        bnode = self.base_node(n1)
        if bnode != self.base_node(n2):
            raise InvalidInflation("Cannot merge {n1} and {n2} since they are not copies of the same node.",
                               invalid_operation="merge",
                               invalid_object=(n1, n2))
        # Check if they have the same parent only if safe is True.
        if safe:
            p1 = set(self.dag.parents(n1))
            p2 = set(self.dag.parents(n2))
            if p1 != p2:
                raise InvalidInflation("Cannot merge {n1} and {n2} because they have different set of parents (and safe=True).",
                                   invalid_operation="merge",
                                   invalid_object=(n1, n2))
        
        # Find who has the lowest copy index (conventionally, for the first var)
        i1, i2 = (self._copy_index[bnode[0]].index(n) for n in (n1,n2))
        n1, n2 = (n1, n2) if i2 < i1 else (n2, n1)
        # Add children of n1 to n2 if any and remove n1
        c2 = list(self.dag.children(n2))
        for c in self.dag.children(n1):
            if not c in c2:
                self.dag.add_link(n2, c)
        for bn, rn in zip(bnode, self.dag.get_variables(n1)):
            self._copy_index[bn].remove(rn)
        self.dag.remove_node(n1)
        self._clear_injectable_sets()
        self._clear_expressible_sets()
    
    def _build_injection_graph(self):
        inj_graph = Graph(directed=False)
        inj_graph.vp.var = inj_graph.new_vertex_property('object')
        ig_index = {}
        # Add vertices
        for node in self.dag.get_nodes(return_all=True):
            for n in node:
                dv = inj_graph.add_vertex()
                inj_graph.vp.var[dv] = n
                ig_index[n] = dv
        
        # Add edges
        for node1, node2 in combinations(self.dag.get_nodes(return_all=True), 2):
            if self._is_injectable([node1[0], node2[0]]):
                for n1, n2 in product(node1, node2):
                    inj_graph.add_edge(ig_index[n1], ig_index[n2])
        return inj_graph #, ig_index
    
    def _build_injectable_sets(self):
        inj_graph = self._build_injection_graph()
        self._injectable_sets = []
        # Get all the maximal cliques
        for clique in chain(max_cliques(inj_graph), (array([i,]) for i in inj_graph.get_vertices())):
            self._injectable_sets.append({inj_graph.vp.var[i] for i in clique}) 
    
    @property
    def injectable_sets(self):
        """List of injectable sets. If it was not previously generated it will be
        generated on the run.
        """
        if self._injectable_sets is None:
            self._build_injectable_sets()
        return self._injectable_sets
    
    def _clear_injectable_sets(self):
        self._injectable_sets = None
    
    def _build_expressibility_graph(self):
        # Build d-separation graph for injectable sets
        exp_graph = Graph(directed=False)
        exp_graph.vp.set = exp_graph.new_vertex_property('object')
        eg_index = {}
        vertex_subsets = []
        # Add vertex for each subsets
        for inj_set in self.injectable_sets:
            vertex_subsets.append([])
            for subset in chain.from_iterable(combinations(inj_set, r) for r in range(1, len(inj_set)+1)):    
                if subset in eg_index:
                    vertex_subsets[-1].append(eg_index[subset])
                else:
                    sv = exp_graph.add_vertex()
                    exp_graph.vp.set[sv] = subset
                    eg_index[subset] = sv
                    vertex_subsets[-1].append(sv)
        # Add edges between subsets of different maximal injectable sets
        for subset1, subset2 in combinations(vertex_subsets, 2):
            for v1, v2 in product(subset1, subset2):
                if self.dag.d_separated(exp_graph.vp.set[v1], exp_graph.vp.set[v2]):
                    exp_graph.add_edge(v1, v2)
        return exp_graph
    
    def _build_expressible_sets(self):
        exp_graph = self._build_expressibility_graph()  
        # Identify maximal expressible sets
        expressible_sets = []
        for clique in chain(max_cliques(exp_graph), (array([i,]) for i in exp_graph.get_vertices())):
            expset = set().union(*[exp_graph.vp.set[i] for i in clique])
            expressible_sets.append(expset)
        self._all_expressible_sets = sorted(expressible_sets, key=lambda s: len(s), reverse=True)
    
    def _build_maximal_expressible_sets(self):
        if self._all_expressible_sets is None:
            self._build_expressible_sets()
        
        maximal_sets = []
        for i in range(1, len(self._all_expressible_sets)+1):
            s1 = self._all_expressible_sets[-i]
            issub = False
            for s2 in self._all_expressible_sets[:-i]:
                if s1.issubset(s2):
                    issub = True
                    break
            if not issub and len(s1) > 1:
                maximal_sets.append(s1) 
        self._maximal_expressible_sets = maximal_sets
    
    @property
    def all_expressible_sets(self):
        if self._all_expressible_sets is None:
            self._build_expressible_sets()
        return self._all_expressible_sets
    
    @property
    def expressible_sets(self):
        if self._maximal_expressible_sets is None:
            self._build_maximal_expressible_sets()
        if not self._explicit_expressible_sets is None:
            return self._explicit_expressible_sets
        else:
            return self._maximal_expressible_sets
    
    @expressible_sets.setter
    def expressible_sets(self, expressible_sets):
        if self._maximal_expressible_sets is None:
            self._build_maximal_expressible_sets()
        self._explicit_expressible_sets = []
        for es1 in expressible_sets:
            for es2 in self._maximal_expressible_sets:
                if es1.issubset(es2):
                    self._explicit_expressible_sets.append(es1)
                    break
    
    def _clear_expressible_sets(self):
        self._all_expressible_sets = None
        self._maximal_expressible_sets = None
        self._explicit_expressible_sets = None
            
    def _is_injectable(self, nodes):
        """Checks injectability directly, without the relying on the cached injectable sets.
        """
        fsub_g = self.dag.new_vertex_property('bool', val=False)
        subverts = set()
        for n in nodes:
            vn = self.dag._vertex(n)
            subverts.add(vn)
            fsub_g[vn] = True
            for a in self.dag.ancestors(n):
                va = self.dag._vertex(a)
                subverts.add(va)
                fsub_g[va] = True
        
        subverts = list(subverts)
        bnodes = []
        fsub_bg = self.base_dag.new_vertex_property('bool', val=False)
        bg_index = self.base_dag.new_vertex_property('int')
        g_index = self.dag.new_vertex_property('int')
        for i, v in enumerate(subverts):
            g_index[v] = i
            n = self.dag.vp.base_node[v][0]
            # If the subset contains copies of the same node
            # we already know is not injectable.
            if n in bnodes:
                return False
            bv = self.base_dag._vertex(n)
            fsub_bg[bv] = True
            bg_index[bv] = i
        
        sub_g = GraphView(self.dag, vfilt=fsub_g)
        sub_bg = GraphView(self.base_dag, vfilt=fsub_bg)
    
        d = similarity(sub_g, sub_bg, 
                label1=g_index,
                label2=bg_index,
                norm=False,
                distance=True)
        return not d
    
    def is_injectable(self, nodes):
        for inj_set in self.injectable_sets:
            if inj_set.issuperset(nodes):
                return True
        return False
    
    def inject(self, nodes):
        if self.is_injectable(nodes):
            for n in nodes:
                yield self.base_variable(n)
        else:
            raise NotInjectable(nodes)
        

