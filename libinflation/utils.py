from itertools import combinations
from sympy import Symbol, Mul, Add, Pow
from sympy.physics.quantum import Operator, HermitianOperator
from graph_tool.generation import complete_graph

def copy_index_combinations(inflation, exclude_latent=True):
    subs_dicts = [{}]
    for base_var, copy_list in inflation._copy_index.items():
        if exclude_latent and inflation.base_dag.is_latent(base_var):
            continue
        new_subs_dicts = []
        for subs in subs_dicts:
            for copy_var in copy_list:
                new_subs = subs.copy()
                new_subs[base_var] = copy_var
                new_subs_dicts.append(new_subs)
            subs_dicts = new_subs_dicts
    return subs_dicts

def copy_index_var_commutations(inflation):
    subs = {}
    for base_var, copy_list in inflation._copy_index.items():
        if inflation.base_dag.is_latent(base_var):
            continue
        for x1,x2 in combinations(copy_list, 2):
            subs[x2*x1] = x1*x2
    return subs

def convert_subs_to_copy_index(subs, inflation, exclude_latent=True):
    new_subs = {}
    for cidx_subs in copy_index_combinations(inflation, exclude_latent=exclude_latent):
        for k, v in subs.items():
            new_subs[k.subs(cidx_subs)] = v.subs(cidx_subs)
    return new_subs

def is_number(expr):
    return isinstance(expr, (int, float, complex)) or expr.is_number

def complement_graph(g):
    """Computes the complement graph of g.
    """
    gc = complete_graph(g.num_vertices())
    
    # Copy only vertex properties to the complement graph
    for (ptype, name), prop in g.properties.items():
        if ptype == 'v':
            new_prop = gc.copy_property(prop)
            gc.vertex_properties[name] = new_prop
    
    for v1, v2 in g.edges():
        gc.remove_edge(gc.edge(v1, v2))
    return gc

def str_index_generator(s, i):
    """Generate the i-th node index from string `s` simply by appending 
    the integer i at the end.
    """
    if isinstance(s, srt):
        return s + str(i)
    else:
        raise ValueError(f"Current index generator cannot deal with objects of type {type(s)}")

def sym_index_generator(s, i):
    """Try to automagically generate the i-th node index from `s`.
    
    Argument `s` should be a sympy object. The function will generate the corresponding 
    new object of the same type and maintaining the assumptios of the original one.
    Only the following base classes are supported: Symbol, HermitianOperator and Operator. 
    If `s` is a sympy expression of the above it updates each object in the expression.
    """
    if isinstance(s, Symbol):
        new_name = s.name + str(i)
        new_s = Symbol(new_name, **s.assumptions0)
        
    elif isinstance(s, HermitianOperator):
        new_symbol = sym_index_generator(s.args[0], i)
        new_s = HermitianOperator(new_symbol)
   
    elif isinstance(s, Operator):
        new_symbol = sym_index_generator(s.args[0], i)
        new_s = Operator(new_symbol)
    
    elif isinstance(s, Add) or isinstance(s, Mul) or isinstance(s, Pow):
        new_args_subs = {}
        for a in s.args:
            new_args_subs[a] = sym_index_generator(a, i)
        new_s = s.copy()
        new_s.subs(new_args_subs)                
        
    else:
        raise ValueError(f"Current index generator cannot deal with objects of type {type(s)}")
    
    return new_s

def index_generator(s, i):
    if isinstance(s, str):
        return str_index_generator(s, i)
    else:
        return sym_index_generator(s, i)
    
def sort_symbols(vs):
    if not isinstance(vs, list):
        vs = list(vs)
    return sorted(vs, key=lambda x: str(x))
