# -*- coding: utf-8 -*-
"""
The module contains functions for applying the scalar extension method.

@author: Davide Poderini
"""

from ncpol2sdpa.nc_utils import get_all_monomials, apply_substitutions, \
    generate_variables, is_number_type

from itertools import product, chain
from functools import reduce, partial
from operator import mul
from graph_tool import Graph, GraphView
from graph_tool.topology import label_components, max_independent_vertex_set 
from graph_tool.util import find_vertex
from graph_tool.generation import complete_graph
from .utils import complement_graph
import sympy as sp
from math import floor

def separability_graph_from_monomials(separability_relations, monomials = None):
    """Return a graph with sympy expressions as vertex property, encoding the 
    separability relationships between the monomials from a list of edges.
    
    Parameters
    ----------
    sep_rel: List of couples of :class:`sympy.core.expr.Expr`
        The independence relationships between the variables.
        Expressed as a list of edges, represented by a couple of independent variables.
    monomials: List of :class:`sympy.core.expr.Expr`
        List of additional disconnected monomials. If None, only the ones 
        appearing in `sep_rel` are going to be included in the graph.
    
    Returns
    -------
    The separability graph as :class:`graph_tool.Graph`.
    """
    g = Graph(directed=False)
    m_prop = g.new_vertex_property("object")
    for m1, m2 in separability_relations:
        if not m1 in m_prop:
            v1 = g.add_vertex()
            m_prop[v1] = m1
        else:
            v1 = find_vertex(g, m_prop, m1)[0]
            
        if not m2 in m_prop:
            v2 = g.add_vertex()
            m_prop[v2] = m2
        else:
            v2 = find_vertex(g, m_prop, m2)[0]
            
        g.add_edge(v1,v2)
    
    # Additional monomials
    if not monomials is None:
        for m in monomials:
            if not find_vertex(g, m_prop, m):
                v = g.add_vertex()
                m_prop[v] = m
                
    g.vp.var= m_prop
    return g

def build_separability_graph(base_graph, substitutions, degree, verbose=False):
    """Build the graph encoding the separability relationships up to a certain degree
    starting from the graph for the base monomials.
    
    Parameters
    ----------
    base_graph: `graph_tool.Graph`
        Base graph with separability relationships between the base monomials.
    
    degree: int
        Specifies to which degree of the base monomials the separability
        graph should be generated.
    
    Returns
    -------
    The separability graph as :class:`graph_tool.Graph`.
    """
    
    if base_graph.is_directed():
        raise RuntimeError("base_graph should be an undirected graph.")
    if not "var" in [p[1] for p in base_graph.properties.keys()]:
        raise RuntimeError("base_graph does not have the vertex property for storing variables.")
        
    # Get all the monomials up to degree and build the new separation graph
    sep_g = Graph(directed=False)
    sep_g.vp.var= sep_g.new_vertex_property('object')
    variables = [base_graph.vp.var[v] for v in base_graph.vertices()]
    monomials = get_all_monomials(variables, None, substitutions, degree)
    
    # Add vertices
    nbase_graph = complement_graph(base_graph)
    msubg = nbase_graph.new_vertex_property('bool')
    for mon in monomials:
        if is_number_type(mon):
            continue
        
        # Only add vertices for nonseparable monomials
        vars = mon.free_symbols
        #if isinstance(mon, sp.Mul):
        if len(vars) > 1:
            for m in vars:
                vm = find_vertex(nbase_graph, nbase_graph.vp.var, m)[0]
                msubg[vm] = True
            nbase_graph.set_vertex_filter(msubg)
            _, hist = label_components(nbase_graph)
            nbase_graph.clear_filters()
            if len(hist) == 1:
                mv = sep_g.add_vertex()
                sep_g.vp.var[mv] = mon
        else:
            mv = sep_g.add_vertex()
            sep_g.vp.var[mv] = mon
    
    # Add egdes
    # TODO: This is a huge bottleneck, optimize this.
    verts = list(sep_g.vertices())
    if verbose: print(f"Dependency graph: #Vertices = {len(verts)}")
    if verbose:
        ecount = 0
        etot = int(len(verts)*(len(verts)-1)/2)
        eprint = 0
    for i in range(len(verts)):
        for j in range(i+1, len(verts)):
            if verbose:
                ecount += 1
                ex = ecount/etot
                if floor(ex*10e4) - eprint > 0: 
                    print(f"\rSeparability graph: Adding edges {ecount}/{etot}, {100*ex:.2f}%", end="")
                    eprint = floor(10e4*ex) 
            v1, v2 = verts[i], verts[j] 
            m1, m2 = sep_g.vp.var[v1], sep_g.vp.var[v2]                
            margs1 = m1.free_symbols
            margs2 = m2.free_symbols
        
            is_sep = True
            for ma1 in margs1:
                va1 = find_vertex(base_graph, base_graph.vp.var, ma1)[0]
                neighbours1 = [base_graph.vp.var[v] for v in va1.out_neighbours()]
                for ma2 in margs2:
                    if not ma2 in neighbours1:
                        is_sep = False
                        break
                if not is_sep: break
            if is_sep:       
                sep_g.add_edge(v1, v2)       
    
    # Remove isolated vertices
    isolated = sep_g.new_vertex_property('bool')
    for v in sep_g.vertices():
        if len(list(v.out_neighbours())) == 0 :
            isolated[v] = True
    sep_g.set_vertex_filter(isolated, inverted=True)
    sep_g.purge_vertices()        
    # return the new separation graph
    #remove_parallel_edges(new_sep_g) # should not be needed
    if verbose: print("\nSeparability graph: done.")
    return sep_g

def get_scalar_extension_variables(separability_graph):
    """Return a dictionary containing the extension variables and their corresponding monomials
    from a given separability graph. Saves as graph property `mvis` the corresponding set of vertices
    found.
    
    :param separation_graph: Graph containing the independence relationships between the variables.
        Expressed as a list of edges, represented by a couple of independent variables.
    :type sep_graph: :class:`graph_tool.Graph`
    
    :returns: Dictionary with extension variables and their corresponding monomials, as :class:`sympy.core.expr.Expr`.
    """
    # Filter isolated vertices
    isolated = separability_graph.new_vertex_property('bool')
    for v in separability_graph.vertices():
        if len(list(v.out_neighbours())) == 0: isolated[v] = True
    separability_graph.set_vertex_filter(isolated, inverted=True)
    
    # Get a maximal independent set (the smaller the better) 
    # and assign the extension variables to them.
    separability_graph.vp.mvis = max_independent_vertex_set(separability_graph, high_deg=True)
    separability_graph.set_vertex_filter(separability_graph.vp.mvis)
    ind_vertices = list(separability_graph.vertices())
    separability_graph.clear_filters()
               
    ext_vars_dict = {}
    ext_vars = generate_variables("α", n_vars=len(ind_vertices))
    for v, ev in zip(ind_vertices, ext_vars):
        ext_vars_dict[separability_graph.vp.var[v]] = ev
    return ext_vars_dict

def split_vars(vars, dependency_graph): 
        independent_vars = []
        msubg = dependency_graph.new_vertex_property('bool')
        for m in vars:
            found = find_vertex(dependency_graph, dependency_graph.vp.var, m)
            if len(found) == 1:
                msubg[found[0]] = True
            elif len(found) == 0:
                independent_vars.append(m)
            else:
                raise RuntimeError("Duplicated vertices in dependency graph.")    
    
        fdg = GraphView(dependency_graph, vfilt=msubg)
        comp, hist = label_components(fdg)
        splitted = [[] for _ in range(len(hist))]
        for v in fdg.vertices():
            splitted[comp[v]].append(fdg.vp.var[v])
        splitted.extend(([v] for v in independent_vars))
        return splitted

def scalar_extension_substitute(monomial, dependency_graph, extension_variables, substitutions):
    """Generate the substitutions constraints for all the scalar extension variables from
    a given dependency graph (complement of a separability graph).
    
    Parameters
    ----------
    monomial: sympy.core.expr.Expr
        Monomial to be substituted
    sep_graph: graph_tool.Graph
        Graph containing the dependence relationships between the base variables.
    extension_variables: dictionary of sympy.core.expr.Expr
        Extension variables as dictionary.
    substitutions: dictionary of sympy.core.expr.Expr
        Additional substitutions to perform.
    
    Returns
    -------   
    Substituted monomial as sympy.core.expr.Expr.
    """

    if is_number_type(monomial):
        return monomial
        
    elif monomial.is_Add:
        result = 0
        for m in monomial.args:
            result += scalar_extension_substitute(arg, dependency_graph, extension_variables, substitutions)
    else:
        #monomial = monomial.subs(substitutions)
        monomial = apply_substitutions(monomial, substitutions, True)
        if monomial in extension_variables:
            result = extension_variables[monomial]
        elif monomial.is_Mul:
            splitted_vars = split_vars(monomial.free_symbols, dependency_graph)
            if len(splitted_vars) > 1:
                result = 1
                for vars in splitted_vars:
                    dep_set = set(vars)
                    collected_args = []
                    for a in monomial.args:
                        if a.free_symbols.issubset(dep_set):
                            collected_args.append(a)
                    #collected_args.reverse()
                    arg = reduce(mul, collected_args, 1)
                    result *= scalar_extension_substitute(arg, dependency_graph, extension_variables, substitutions)
            else:
                if monomial in extension_variables:
                    result = extension_variables[monomial]
                else:
                    result = monomial
        else:
            result = monomial
    #return result.subs(substitutions) 
    return apply_substitutions(result, substitutions, True)
    
    #elif isinstance(monomial, sp.Mul):
    #    msubg = dependency_graph.new_vertex_property('bool')
    #    independent_monomials = [] 
    #    for m in monomial.args:
    #        found = find_vertex(dependency_graph, dependency_graph.vp.var, m)
    #        if len(found) == 1:
    #            msubg[found[0]] = True
    #        elif len(found) == 0:
    #            independent_monomials.append(m)
    #        else:
    #            raise ValueError("Duplicated vertices in dependency graph.")    
    #    dependency_graph.set_vertex_filter(msubg)
    #    comp, hist = label_components(dependency_graph)
    #    comp_mon = [1]*len(hist)
    #    
    #    # TODO: This destroys non-commutativity!!!
    #    for v in dependency_graph.vertices():
    #        comp_mon[comp[v]] *= dependency_graph.vp.var[v]
    #    dependency_graph.clear_filters()
    #    result = 1
    #    
    #    # TODO: This destroys non-commutativity!!!
    #    for m in chain(comp_mon, independent_monomials):
    #        m = apply_substitutions(m, substitutions, True)
    #        if m in extension_variables:
    #            result *= extension_variables[m]
    #        else:
    #            result *= m
    #    result = apply_substitutions(result, substitutions, True)
    #    
    #return result

def _assemble_moments(monomials, substitutions):
    for m1, m2 in product(monomials, repeat=2): 
        monomial = m1.adjoint() * m2
        monomial = apply_substitutions(monomial, substitutions, True)
        yield monomial

def get_scalar_extension_substitutions(separability_graph, degree, 
        substitutions=None, 
        extension_variables=None, 
        return_extension_variables=False):
    
    """Explicitly generates all the moment substitutions constraints for the scalar extension 
    variables from a given separability graph. 
    This is usually computationally expensive for large matrices, use directly a function as
    moment substitution in those cases.
    
    :param sep_graph: Graph containing the independence relationships between the monomials.
    :type sep_graph: :class:`graph_tool.Graph`
    :param extension_variables: Optional parameter to explicitly specify the extension variables 
        as dict of monomials and expressions.
    :type extension_variables: dict of :class:`sympy.core.expr.Expr`    
    :param return_extension_variables: Optional parameter to specify whether to return the constraints and the 
        extension variables as two different `dict`.
    :type return_extension_variables: bool
    
    :returns: Dictionary with substitution constraints for the scalar extension variables, as :class:`sympy.core.expr.Expr`.
    """
    if extension_variables is None:
        g = build_separability_graph(separability_graph, substitutions, degree)
        extension_variables = get_scalar_extension_variables(g)
        monomials = [g.vp.var[v] for v in g.vertices()]
    else:
        monomials = get_all_monomials(variables, None, substitutions, degree)
        
    dependency_graph = complement_graph(separability_graph)
    
    subs = {}
    for m in _assemble_moments(monomials, substitutions):
        s = scalar_extension_substitute(m, dependency_graph, extension_variables, substitutions)
        if s != m:
            subs[m] = s
    if return_extension_variables:
        return subs, extension_variables
    else:
        subs.update(extension_variables)
        return subs

def generate_scalar_extension_constraints(separability_relations, 
        substitutions,
        monomials=None, 
        degree=1, 
        extension_variables=None):
    """Generate scalar extension constraints directly from the separation relationships, expressed
    as a list of couples of monomials.
    
    :param separability_relations: The independence relationships between the variables.
        Expressed as a list of edges, represented by a couple of independent variables.
    :type separability_relations: List of couples of :class:`sympy.core.expr.Expr`.
    :param monomials: List of additional disconnected monomials. If None, only the ones 
        appearing in `separability_relations` are going to be included in the graph.
    :type monomials: List of :class:`sympy.core.expr.Expr`
    :param degree: Specifies to which degree of the base monomials the separability
        graph should be generated.
    :type degree: int
    :param extension_variables: Optional parameter to explicitly specify the extension variables,
        as list of monomials, or as a dictionary.
    :type extension_variables: list or dict of :class:`sympy.core.expr.Expr`
    
    :returns: Dictionary with substitution constraints and a list of the scalar extension 
        variables, as :class:`sympy.core.expr.Expr`.
    """
    if isinstance(extension_variables, list):
        αs = generate_variables("α", n_vars=len(extension_variables))
        extension_variables = {m:α for m, α in zip(extension_variables, αs)}
    
    # Generate the separation graph for base variables
    base_graph = separability_graph_from_monomials(separability_relations, monomials)
    subs, evars = get_scalar_extension_substitutions(base_graph, degree,  
        substitutions=substitutions, 
        extension_variables=extension_variables, 
        return_extension_variables=True)
    
    #evar_list = list(set(evars.values()))
    subs.update(evars)
    
    return subs, evars

def generate_scalar_extension_substitution_function(separability_relations, 
        substitutions,
        monomials=None,
        degree=1,
        extension_variables=None):
    
    """Generate substitution function for imposing scalar extension constraints to
    moments, directly from the separation relationships, expressed
    as a list of couples of monomials.
    
    Parameters
    ----------
    separability_relations: List of couples of :class:`sympy.core.expr.Expr`
        The independence relationships between the variables.
        Expressed as a list of edges, represented by a couple of independent variables.
    monomials: List of `sympy.core.expr.Expr`
        List of additional disconnected monomials. If None, only the ones 
        appearing in `separability_relations` are going to be included in the graph.
    degree: int 
        Specifies to which degree of the base monomials the separability
        graph should be generated.
    extension_variables: List or dict of `sympy.core.expr.Expr` 
        Optional parameter to explicitly specify the extension variables,
        as list of monomials, or as a dictionary.
    
    Returns
    ------- 
    A function that takes one argument, the monomial, and returns the
    corresponding subtitution.
    """
    
    if isinstance(extension_variables, list):
        αs = generate_variables("α", n_vars=len(extension_variables))
        extension_variables = {m:α for m, α in zip(extension_variables, αs)}
    
    # Generate the separation graph for base variables
    base_graph = separability_graph_from_monomials(separability_relations, monomials)
    # Generate or collect the extension variables and monomials
    if extension_variables is None:
        g = build_separability_graph(base_graph, substitutions, degree)
        extension_variables = get_scalar_extension_variables(g)
        monomials = [g.vp.var[v] for v in g.vertices()]
    else:
        variables = list(extension_variables.keys())
        monomials = get_all_monomials(variables, None, substitutions, degree)
        
    dependency_graph = complement_graph(base_graph)
    
    subfunc = partial(scalar_extension_substitute, 
        dependency_graph=dependency_graph, 
        extension_variables=extension_variables,
        substitutions=substitutions)
    
    #evar_list = list(set(extension_variables.values()))
    return subfunc, extension_variables
