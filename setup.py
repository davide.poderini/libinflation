from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="libinflation",
    description="Library to solve optimization problems with causal constraints",
    version="0.1",
    packages=find_packages(),
    install_requires=['docutils>=0.3'],
    long_description=long_description,
    long_description_content_type="text/markdown",

    #scripts = [ ],

    author="Davide Poderini",
    author_email="davide@poder.in",
)
