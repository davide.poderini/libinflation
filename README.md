# Libinflation

A library to solve optimization problems with causal constraints, both for
classical and quantum systems.

It makes use of the inflation technique [1] and scalar extension [2] to enforce
causal constraints, and convert the optimization problem to an LP or an SDP
using the ncpod2sdpa library for bulding the approriate relaxation.
